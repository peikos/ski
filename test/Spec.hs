import Test.Hspec
import Test.QuickCheck

import Lib

main :: IO ()
main = hspec $ do 
  describe "SKI" $ do
    it "evaluates to S" $ do
      evaluate (K :.: S :.: I) `shouldBe` S

  describe "SKK" $ do
    it "evaluates to I" $ do
      evaluate (S :.: K :.: K) `shouldBe` I

  describe "SKS" $ do
    it "evaluates to I" $ do
      evaluate (S :.: K :.: S) `shouldBe` I

  describe "SKKK" $ do
    it "evaluates to K" $ do
      evaluate (S :.: K :.: K :.: K) `shouldBe` K

  describe "SKKS" $ do
    it "evaluates to S" $ do
      evaluate (S :.: K :.: K :.: S) `shouldBe` S

  describe "SKSK" $ do
    it "evaluates to K" $ do
      evaluate (S :.: K :.: S :.: K) `shouldBe` K

  describe "SKSS" $ do
    it "evaluates to S" $ do
      evaluate (S :.: K :.: S :.: S) `shouldBe` S

  describe "λx[xSK](I)" $ do
    it "evaluates to SK" $ do
      evaluate ((Lambda 'x' $ Var 'x' :.: S :.: K) :.: I) `shouldBe` (S1 K)

  describe "ISK" $ do
    it "evaluates to SK" $ do
      evaluate (I :.: S :.: K) `shouldBe` (S1 K)

  describe "λx[λy[λz[xz(yz)]]](I)(S)" $ do
    it "evaluates to λz[z(Sz)]" $ do
      evaluate (Lambda 'x'
                 (Lambda 'y'
                   (Lambda 'z' (Var 'x' :.: Var 'z' :.: (Var 'y' :.: Var 'z'))))
                :.: I :.: S )
            `shouldBe`
               (Lambda 'z' (Var 'z' :.: (S1 (Var 'z'))))
