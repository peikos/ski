{-# LANGUAGE OverloadedStrings #-}

module Lib where

import Control.Applicative
import Control.Monad
import Data.Maybe
import Data.Text

import Data.Attoparsec.Text (parseOnly, Parser, string, anyChar)

data Combinator = S | K | I | Combinator :.: Combinator
                | K1 Combinator
                | S1 Combinator | S2 Combinator Combinator 
                | Var Char | Lambda Char Combinator
  deriving (Eq, Show)

-- Parsing
parseS, parseK, parseI :: Parser Combinator
parseS = S <$ string "s"
parseK = K <$ string "k"
parseI = I <$ string "i"

parseVar :: Parser Combinator
parseVar = do c <- anyChar
              guard $ c `elem` ("abcdefghjlmnopqrtuvwxyz" :: [Char])
              return $ Var c

parseParens :: Parser Combinator
parseParens = string "(" *> parseString Nothing <* string ")"

-- Use accumulator a for left instead of right associativity
parseString :: Maybe Combinator -> Parser Combinator
parseString a = do mc <- Just <$> parseSubExpr <|> return Nothing
                   case mc of
                     Just c -> parseString $ Just $ maybe c (:.: c) a
                     Nothing -> return $ fromMaybe I a

parseLambda :: Parser Combinator
parseLambda = do _ <- string "l"
                 v <- anyChar
                 b <- string "[" *> parseString Nothing <* string "]"
                 return $ Lambda v b

parseSubExpr = parseS <|> parseK <|> parseI <|> parseLambda <|> parseVar <|> parseParens

-- Recursive beta reduction
sub :: Combinator -> Combinator -> Combinator -> Combinator
-- Substitute if left operand is to-be-replaced variable. Call recursively on subtrees.
sub var repl (a :.: b) | var == a   = repl           :.: sub var repl b
                       | otherwise  = sub var repl a :.: sub var repl b
-- Propagate into lambdas
sub var repl (Lambda lvar lbody) = Lambda lvar $ sub var repl lbody
-- Propagate into curryd K/S
sub var repl (K1 x) = K1 $ sub var repl x
sub var repl (S1 x) = S1 $ sub var repl x
sub var repl (S2 x y) = S2 (sub var repl x) (sub var repl y)
-- If atomary combinator: only replace if to-be-replaced variable, otherwise stop recursion.
sub var repl comb | var == comb = repl
                  | otherwise   = comb

-- Evaluation
evaluate :: Combinator -> Combinator
-- Basic combinators
evaluate S = S
evaluate K = K
evaluate I = I
-- Applied combinators
evaluate (S1 x) = S1 $ evaluate x
evaluate (S2 x y) = S2 (evaluate x) (evaluate y)
evaluate (K1 x) = K1 $ evaluate x
-- Remaining (free) variables
evaluate (Var x) = Var x
-- Unevaluated lambdas
evaluate (Lambda x c) = Lambda x $ evaluate c
--  Beta reduction
evaluate (Lambda x c :.: p) = evaluate $ sub (Var x) p c
-- Simplify SK to I for readability
evaluate (S :.: K :.: _) = I
-- Apply Identity
evaluate (I :.: x) = evaluate x
-- Apply Constant in two steps (currying)
evaluate (K :.: c) = K1 (evaluate c)
evaluate (K1 c :.: _) = evaluate c
-- Apply Substition in three steps (currying)
evaluate (S :.: c) = S1 (evaluate c)
evaluate (S1 x :.: y) = S2 x (evaluate y)
evaluate (S2 x y :.: z) = evaluate (evaluate (x :.: z) :.: evaluate (y :.: z))
-- If stuck try evaluating subtrees first (call by name / value)
evaluate (x :.: y) = let x' = evaluate x         -- Try evaluating both subtrees
                         y' = evaluate y
                     in if x == x' && y == y'
                       then x :.: y              -- If nothing changed, quit trying
                       else evaluate $ x' :.: y' -- Else try again with simplified subtrees

readEvalPrint :: IO () -- ToDo: replace 2nd show by pretty show
readEvalPrint = putStrLn =<< either show (show . evaluate)
                        . parseOnly (parseString Nothing)
                        . pack
                        <$> getLine
